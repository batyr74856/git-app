import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sputnik_app/pages/followers/ui/followers_screen.dart';

import 'package:sputnik_app/pages/home/ui/home_screen.dart';
import 'package:sputnik_app/pages/search/ui/search_screen.dart';
import 'package:sputnik_app/translations/locale_keys.g.dart';

import 'core/app_assets.dart';
import 'core/app_colors.dart';
import 'pages/profile/ui/profile_screen.dart';

class BottomNavigationBarPage extends StatefulWidget {
  const BottomNavigationBarPage({Key? key}) : super(key: key);

  @override
  State<BottomNavigationBarPage> createState() =>
      _BottomNavigationBarPageState();
}

List pages = [
  const MyHomePage(),
  FollowersScreen(),
  const SearchScreen(),
  const ProfileScreen()
];

class _BottomNavigationBarPageState extends State<BottomNavigationBarPage> {
  int selectedIndex = 0;
  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColors.background,
      body: pages[selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppAssets.home,
              height: 20,
              color:
                  selectedIndex == 0 ? AppColors.blackPrimary : AppColors.grey,
            ),
            label: LocaleKeys.home_text.tr(),
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppAssets.users,
              height: 20,
              color:
                  selectedIndex == 1 ? AppColors.blackPrimary : AppColors.grey,
            ),
            label: LocaleKeys.followers_text.tr(),
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppAssets.send,
              height: 20,
              color:
                  selectedIndex == 2 ? AppColors.blackPrimary : AppColors.grey,
            ),
            label: LocaleKeys.chat_text.tr(),
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppAssets.profile,
              height: 21,
              color:
                  selectedIndex == 3 ? AppColors.blackPrimary : AppColors.grey,
            ),
            label: LocaleKeys.profile_text.tr(),
          ),
        ],
        currentIndex: selectedIndex,
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        elevation: 0,
        unselectedLabelStyle: TextStyle(
          fontFamily: 'Poppins',
          fontSize: 10,
          color: AppColors.grey,
          fontWeight: FontWeight.w700,
        ),
        fixedColor: AppColors.blackPrimary,
        selectedLabelStyle: TextStyle(
          fontFamily: 'Poppins',
          fontSize: 10,
          color: AppColors.blackPrimary,
          fontWeight: FontWeight.w700,
        ),
        iconSize: 20,
        onTap: onItemTapped,
      ),
    );
  }
}
