class AppAssets {
  AppAssets._();

  static const String logo = 'assets/images/logo.png';
  static const String home = 'assets/icons/home.svg';
  static const String users = 'assets/icons/users.svg';
  static const String send = 'assets/icons/send.svg';
  static const String profile = 'assets/icons/profile.svg';
  static const String bell = 'assets/icons/bell.svg';
  static const String setting = 'assets/icons/setting.svg';
  static const String heart = 'assets/icons/heart.svg';
  static const String medal = 'assets/icons/medal.svg';
  static const String dollar = 'assets/icons/dollar.svg';
  static const String filter = 'assets/icons/filter.svg';
  static const String plus = 'assets/icons/plus.svg';
  static const String image = 'assets/images/image.png';
  static const String star = 'assets/icons/star.svg';
  static const String fork = 'assets/icons/fork.svg';
  static const String russian = 'assets/icons/russian.png';
  static const String united = 'assets/icons/united.png';
  static const String error = 'assets/images/error.svg';
}
