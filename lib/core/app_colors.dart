import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static Color primary = const Color.fromRGBO(254, 144, 75, 1);
  static Color secondary = const Color.fromRGBO(251, 114, 76, 1);
  static Color blackPrimary = const Color.fromRGBO(43, 43, 43, 1);
  static Color white = const Color.fromRGBO(255, 255, 255, 1);
  static Color black = const Color.fromRGBO(64, 64, 64, 1);
  static Color grey = const Color.fromRGBO(174, 174, 178, 1);
  static Color greyPrimary = const Color.fromRGBO(161, 161, 161, 1);
  static Color background = const Color.fromRGBO(252, 252, 252, 1);
  static Color greyBackground = const Color.fromRGBO(240, 240, 240, 1);
  static Color subtitle = const Color.fromRGBO(79, 79, 79, 1);
  static Color dividerColor = const Color.fromRGBO(236, 238, 241, 1);
  static Color goldColor = const Color.fromRGBO(255, 203, 85, 1);
  static Color greyBackgroundSecondary =
      const Color.fromRGBO(229, 229, 234, 0.8);
  static Color searchButtonColor = const Color.fromRGBO(226, 226, 226, 1);
  static Color errorColor = const Color.fromRGBO(231, 58, 64, 1);
}
