import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sputnik_app/data/service_locator.dart';
import 'package:sputnik_app/pages/followers/service/followers_repository.dart';
import 'package:sputnik_app/pages/home/following/service/following_repository.dart';
import 'package:sputnik_app/pages/home/repos/service/repos_repositiry.dart';
import 'package:sputnik_app/pages/profile/bloc/profile_bloc.dart';
import 'package:sputnik_app/pages/profile/service/profile_repository.dart';
import 'package:sputnik_app/pages/search/service/search_repository.dart';
import 'package:sputnik_app/pages/welcome/ui/splash_screen.dart';

import 'pages/followers/bloc/followers_bloc.dart';
import 'pages/home/following/bloc/following_bloc.dart';
import 'pages/home/repos/bloc/repos_bloc.dart';
import 'pages/search/bloc/search_bloc.dart';
import 'translations/codegen_loader.g.dart';

bool show = true;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  final prefs = await SharedPreferences.getInstance();
  serviceLocatorSetup();
  show = prefs.getBool('ON_BOARDING') ?? true;
  runApp(EasyLocalization(
      path: 'assets/translations',
      supportedLocales: const [
        Locale('en'),
        Locale('ru'),
      ],
      fallbackLocale: const Locale("en"),
      assetLoader: const CodegenLoader(),
      child: MyApp()));
}

// ignore: must_be_immutable
class MyApp extends StatelessWidget {
  ProfileProvider profileProvider = ProfileProvider();
  FollowersProvider followersProvider = FollowersProvider();
  ReposProvider reposProvider = ReposProvider();
  FollowingProvider followingProvider = FollowingProvider();
  SearchProvider searchProvider = SearchProvider();
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ProfileBloc>(
          create: ((context) => ProfileBloc(profileProvider: profileProvider)
            ..add(GetUserEvent())),
        ),
        BlocProvider<FollowersBloc>(
          create: ((context) =>
              FollowersBloc(followersProvider: followersProvider)
                ..add(GetFollowersListEvent())),
        ),
        BlocProvider<ReposBloc>(
          create: ((context) => ReposBloc(reposProvider: reposProvider)
            ..add(GetListReposEvent())),
        ),
        BlocProvider<FollowingBloc>(
          create: ((context) =>
              FollowingBloc(followingProvider: followingProvider)
                ..add(GetListFollowingEvent())),
        ),
        BlocProvider<SearchBloc>(
          create: ((context) => SearchBloc(searchProvider: searchProvider)
            ..add(Search(query: ''))),
        ),
      ],
      child: MaterialApp(
        title: 'Sputnik App',
        debugShowCheckedModeBanner: false,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        localizationsDelegates: context.localizationDelegates,
        home: const SplashScreen(),
      ),
    );
  }
}
