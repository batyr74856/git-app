import 'package:sputnik_app/data/models/followers.dart';
import 'package:sputnik_app/utils/logger.dart';

import '../../../data/network/rest_client.dart';
import '../../../data/service_locator.dart';

class SearchProvider {
  Future<List<Followers>?> getData(String query, dynamic result) async {
    try {
      var response = await serviceLocator<RestClient>().getFollowers();

      result = response
          .where((element) =>
              element.name!.toLowerCase().contains((query.toLowerCase())))
          .toList();

      return result;
    } catch (e) {
      logger.e(e);
    }
    return null;
  }
}
