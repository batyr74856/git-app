import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:sputnik_app/data/models/followers.dart';
import 'package:sputnik_app/utils/logger.dart';

import '../service/search_repository.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  SearchProvider searchProvider = SearchProvider();

  SearchBloc({required this.searchProvider}) : super(SearchUninitialized()) {
    on<Search>(searchFollowers);
  }

  FutureOr<void> searchFollowers(
      Search event, Emitter<SearchState> emit) async {
    emit(SearchLoading());
    try {
      List<Followers>? getData =
          await searchProvider.getData(event.query, event.followers);
      if (getData == null) {
        return;
      }

      if (event.followers!.isEmpty) {
        emit(SearchError());
      } else {
        emit(SearchLoaded(followers: getData));
      }
    } catch (e) {
      emit(SearchError());
      logger.e(e);
    }
  }
}
