part of 'search_bloc.dart';

abstract class SearchEvent extends Equatable {}

// ignore: must_be_immutable
class Search extends SearchEvent {
  String query;
  List<Followers>? followers;

  Search({required this.query, this.followers});

  @override
  List<Object> get props => [];
}
