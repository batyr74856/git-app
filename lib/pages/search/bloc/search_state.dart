part of 'search_bloc.dart';

abstract class SearchState extends Equatable {}

class SearchUninitialized extends SearchState {
  @override
  List<Object> get props => [];
}

class SearchLoading extends SearchState {
  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class SearchLoaded extends SearchState {
  List<Followers>? followers;
  SearchLoaded({required this.followers});
  @override
  List<Object> get props => [];
}

class SearchError extends SearchState {
  @override
  List<Object> get props => [];
}
