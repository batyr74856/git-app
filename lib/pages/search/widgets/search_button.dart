import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sputnik_app/core/app_colors.dart';

import '../../../data/network/rest_client.dart';
import '../../../data/service_locator.dart';
import '../../../translations/locale_keys.g.dart';
import '../bloc/search_bloc.dart';

class SearchUnButton extends StatelessWidget {
  const SearchUnButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(vertical: 18),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
          color: AppColors.searchButtonColor),
      child: Text(
        LocaleKeys.search_text.tr(),
        style: TextStyle(
          fontFamily: 'Poppins',
          fontSize: 17,
          color: AppColors.white,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }
}

class SearchButton extends StatelessWidget {
  const SearchButton({
    Key? key,
    required this.name,
  }) : super(key: key);

  final String name;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        var response = await serviceLocator<RestClient>().getFollowers();

        final result = response
            .where((element) =>
                element.name!.toLowerCase().contains((name.toLowerCase())))
            .toList();

        // ignore: use_build_context_synchronously
        context.read<SearchBloc>().add(Search(query: name, followers: result));
      },
      child: Container(
        width: double.infinity,
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(vertical: 18),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14),
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                AppColors.primary,
                AppColors.secondary,
              ],
            )),
        child: Text(
          LocaleKeys.search_text.tr(),
          style: TextStyle(
            fontFamily: 'Poppins',
            fontSize: 17,
            color: AppColors.white,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
    );
  }
}
