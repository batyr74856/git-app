import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:sputnik_app/core/app_colors.dart';
import 'package:sputnik_app/translations/locale_keys.g.dart';

class GitHubSocialText extends StatelessWidget {
  const GitHubSocialText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Text(
              LocaleKeys.github_text.tr(),
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 28,
                color: AppColors.blackPrimary,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
        Row(
          children: [
            Text(
              LocaleKeys.enter_nickname_text.tr(),
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 17,
                color: AppColors.grey,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class InfoText extends StatelessWidget {
  const InfoText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              LocaleKeys.sign_in_text.tr(),
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 13,
                color: AppColors.grey,
                fontWeight: FontWeight.w500,
              ),
            ),
            const SizedBox(
              width: 4,
            ),
            Text(
              LocaleKeys.term_text.tr(),
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 13,
                color: AppColors.blackPrimary,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              LocaleKeys.and_text.tr(),
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 13,
                color: AppColors.grey,
                fontWeight: FontWeight.w500,
              ),
            ),
            const SizedBox(
              width: 4,
            ),
            Text(
              LocaleKeys.policy_text.tr(),
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 13,
                color: AppColors.blackPrimary,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
