import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sputnik_app/core/image_network.dart';

import 'package:sputnik_app/pages/search/widgets/search_screen_text.dart';
import 'package:sputnik_app/translations/locale_keys.g.dart';

import '../../../core/app_assets.dart';
import '../../../core/app_colors.dart';
import '../../../data/models/followers.dart';

import '../bloc/search_bloc.dart';
import '../widgets/search_button.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({super.key});

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  String name = '';
  Followers? followers;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColors.background,
      appBar: AppBar(
          elevation: 0,
          backgroundColor: AppColors.background,
          leading: Icon(Icons.arrow_back, color: AppColors.blackPrimary)),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            const GitHubSocialText(),
            const SizedBox(
              height: 40,
            ),
            TextFormField(
              cursorColor: AppColors.blackPrimary,
              onChanged: (value) {
                setState(() {
                  name = value;
                });
              },
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: AppColors.blackPrimary),
              textAlign: TextAlign.left,
              decoration: InputDecoration(
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: AppColors.grey),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  hintText: LocaleKeys.enter_nickname_on_text.tr(),
                  hintStyle: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 25,
                      fontWeight: FontWeight.w400,
                      color: AppColors.greyPrimary),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: BorderSide.none),
                  filled: true,
                  fillColor: AppColors.greyBackground),
            ),
            Expanded(
                child: name != ''
                    ? SizedBox(
                        child: BlocBuilder<SearchBloc, SearchState>(
                          builder: (context, state) {
                            if (state is SearchUninitialized) {
                              return const SizedBox();
                            } else if (state is SearchLoading) {
                              return const Center(
                                  child: CircularProgressIndicator());
                            } else if (state is SearchLoaded) {
                              return ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: state.followers!.length,
                                  itemBuilder: (context, index) {
                                    return Column(
                                      children: [
                                        Divider(
                                          color: AppColors.dividerColor,
                                          thickness: 2,
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        ListTile(
                                          leading: Material(
                                            elevation: 5,
                                            borderRadius:
                                                BorderRadius.circular(60),
                                            child: Container(
                                              height: 58,
                                              width: 58,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          60)),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(60),
                                                child: BaseNetworkImage(
                                                  url:
                                                      '${state.followers![index].avatar}',
                                                  fit: BoxFit.fill,
                                                ),
                                              ),
                                            ),
                                          ),
                                          title: Text(
                                            state.followers![index].name!,
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontSize: 20,
                                              color: AppColors.blackPrimary,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                          subtitle: Text(
                                            '${state.followers![index].id!}',
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontSize: 17,
                                              color: AppColors.subtitle,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                      ],
                                    );
                                  });
                            } else if (state is SearchError) {
                              return const ErrorWidget();
                            }
                            return const SizedBox();
                          },
                        ),
                      )
                    : const SizedBox()),
            name == '' ? const SearchUnButton() : SearchButton(name: name),
            const SizedBox(
              height: 15,
            ),
            const InfoText(),
            const SizedBox(
              height: 15,
            ),
          ],
        ),
      ),
    );
  }
}

class ErrorWidget extends StatelessWidget {
  const ErrorWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset(AppAssets.error),
        const SizedBox(
          height: 10,
        ),
        Text(
          LocaleKeys.error_first_text.tr(),
          style: TextStyle(
            fontFamily: 'Poppins',
            fontSize: 20,
            color: AppColors.errorColor,
            fontWeight: FontWeight.w500,
          ),
        ),
        Text(
          LocaleKeys.error_second_text.tr(),
          style: TextStyle(
            fontFamily: 'Poppins',
            fontSize: 20,
            color: AppColors.errorColor,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );
  }
}
