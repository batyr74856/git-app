part of 'profile_bloc.dart';

abstract class ProfileEvent extends Equatable {}

class GetUserEvent extends ProfileEvent {
  @override
  List<Object> get props => [];
}
