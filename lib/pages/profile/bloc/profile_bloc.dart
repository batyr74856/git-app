import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:sputnik_app/pages/profile/service/profile_repository.dart';
import 'package:sputnik_app/utils/logger.dart';

import '../../../data/models/user.dart';

part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileProvider profileProvider = ProfileProvider();
  ProfileBloc({required this.profileProvider}) : super(ProfileInitial()) {
    on<GetUserEvent>(getUserData);
  }

  FutureOr<void> getUserData(
      GetUserEvent event, Emitter<ProfileState> emit) async {
    emit(ProfileLoading());
    try {
      User? getData = await profileProvider.getData();
      if (getData == null) {
        return;
      }

      emit(ProfileLoaded(user: getData));
    } catch (e) {
      logger.e(e);
    }
  }
}
