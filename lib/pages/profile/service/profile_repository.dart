import 'package:sputnik_app/utils/logger.dart';

import '../../../data/models/user.dart';
import '../../../data/network/rest_client.dart';
import '../../../data/service_locator.dart';

class ProfileProvider {
  Future<User?> getData() async {
    try {
      var response = await serviceLocator<RestClient>().getProfile();
      return response;
    } catch (e) {
      logger.e(e);
    }
    return null;
  }
}
