import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../core/app_assets.dart';

class ChangeLanguage extends StatelessWidget {
  const ChangeLanguage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: () async {
            await context.setLocale(const Locale('ru'));
          },
          child: Image.asset(
            AppAssets.russian,
            height: 29,
            width: 29,
          ),
        ),
        const SizedBox(
          width: 30,
        ),
        InkWell(
          onTap: () async {
            await context.setLocale(const Locale('en'));
          },
          child: Image.asset(
            AppAssets.united,
            height: 28,
            width: 25,
          ),
        )
      ],
    );
  }
}
