import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sputnik_app/core/app_assets.dart';
import 'package:sputnik_app/core/app_colors.dart';
import 'package:sputnik_app/translations/locale_keys.g.dart';

class ProfileBody extends StatelessWidget {
  const ProfileBody({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14), color: AppColors.white),
          child: Row(
            children: [
              SvgPicture.asset(AppAssets.heart),
              const SizedBox(
                width: 10,
              ),
              Text(
                LocaleKeys.my_following_text.tr(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 15,
                  color: AppColors.blackPrimary,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 14,
        ),
        Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14), color: AppColors.white),
          child: Row(
            children: [
              SvgPicture.asset(AppAssets.heart),
              const SizedBox(
                width: 10,
              ),
              Text(
                LocaleKeys.my_followers_text.tr(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 15,
                  color: AppColors.blackPrimary,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 14,
        ),
        Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14), color: AppColors.white),
          child: Row(
            children: [
              SvgPicture.asset(AppAssets.medal),
              const SizedBox(
                width: 10,
              ),
              Text(
                LocaleKeys.my_badges_text.tr(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 15,
                  color: AppColors.blackPrimary,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 14,
        ),
        Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14), color: AppColors.white),
          child: Row(
            children: [
              SvgPicture.asset(AppAssets.dollar),
              const SizedBox(
                width: 10,
              ),
              Text(
                LocaleKeys.my_organizatios_text.tr(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 15,
                  color: AppColors.blackPrimary,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 18,
        ),
        Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(vertical: 15),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              color: AppColors.blackPrimary),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                LocaleKeys.view_all_text.tr(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 17,
                  color: AppColors.white,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Icon(
                Icons.arrow_forward,
                color: AppColors.white,
                size: 18,
              )
            ],
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              onTap: () async {
                await context.setLocale(const Locale('ru'));
              },
              child: Image.asset(
                AppAssets.russian,
                height: 29,
                width: 29,
              ),
            ),
            const SizedBox(
              width: 30,
            ),
            InkWell(
              onTap: () async {
                await context.setLocale(const Locale('en'));
              },
              child: Image.asset(
                AppAssets.united,
                height: 28,
                width: 25,
              ),
            )
          ],
        ),
      ],
    );
  }
}
