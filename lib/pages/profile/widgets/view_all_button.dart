import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:sputnik_app/core/app_colors.dart';
import 'package:sputnik_app/translations/locale_keys.g.dart';

class ViewAllButton extends StatelessWidget {
  const ViewAllButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(vertical: 15),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
          color: AppColors.blackPrimary),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            LocaleKeys.view_all_text.tr(),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Poppins',
              fontSize: 17,
              color: AppColors.white,
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Icon(
            Icons.arrow_forward,
            color: AppColors.white,
            size: 18,
          )
        ],
      ),
    );
  }
}
