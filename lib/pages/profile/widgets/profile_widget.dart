import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sputnik_app/core/app_assets.dart';
import 'package:sputnik_app/core/app_colors.dart';
import 'package:sputnik_app/core/image_network.dart';

import '../bloc/profile_bloc.dart';

class ProfileInfo extends StatelessWidget {
  const ProfileInfo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        if (state is ProfileInitial) {
          return const SizedBox();
        } else if (state is ProfileLoading) {
          return const CircularProgressIndicator();
        } else if (state is ProfileLoaded) {
          return Column(
            children: [
              Material(
                elevation: 5,
                borderRadius: BorderRadius.circular(80),
                child: Container(
                  height: 120,
                  width: 120,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(80)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(80),
                    child: BaseNetworkImage(
                      url: '${state.user?.avatarUrl}',
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                state.user!.name!,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 25,
                  color: AppColors.blackPrimary,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Text(
                '${state.user!.id}',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 15,
                  color: AppColors.grey,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          );
        }
        return const SizedBox();
      },
    );
  }
}

class ProfileAppBar extends StatelessWidget {
  const ProfileAppBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SvgPicture.asset(AppAssets.setting),
        SvgPicture.asset(AppAssets.bell)
      ],
    );
  }
}
