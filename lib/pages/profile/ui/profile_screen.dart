import 'package:flutter/material.dart';

import 'package:sputnik_app/pages/profile/widgets/profile_list.dart';

import '../../../core/app_colors.dart';

import '../widgets/profile_widget.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: SingleChildScrollView(
          child: Column(
            children: const [
              SizedBox(
                height: 10,
              ),
              ProfileAppBar(),
              SizedBox(
                height: 10,
              ),
              ProfileInfo(),
              SizedBox(
                height: 18,
              ),
              ProfileBody(),
            ],
          ),
        ),
      )),
    );
  }
}
