import 'package:flutter/material.dart';

import 'package:sputnik_app/core/app_colors.dart';

import 'package:sputnik_app/pages/home/widgets/following_list.dart';
import 'package:sputnik_app/pages/home/widgets/repositories_list.dart';
import 'package:sputnik_app/pages/home/widgets/user_info.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          children: const [
            SizedBox(
              height: 10,
            ),
            UserInfo(),
            SizedBox(
              height: 10,
            ),
            FollowingList(),
            SizedBox(
              height: 10,
            ),
            RepositoriesList(),
            SizedBox(
              height: 10,
            )
          ],
        ),
      )),
    );
  }
}
