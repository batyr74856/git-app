import 'package:sputnik_app/data/models/repos.dart';

import '../../../../data/network/rest_client.dart';
import '../../../../data/service_locator.dart';
import '../../../../utils/logger.dart';

class ReposProvider {
  Future<List<Repos>?> getData() async {
    try {
      var response = await serviceLocator<RestClient>().getRepos();
      return response;
    } catch (e) {
      logger.e(e);
    }
    return null;
  }
}
