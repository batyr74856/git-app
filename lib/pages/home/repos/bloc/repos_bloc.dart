import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:sputnik_app/data/models/repos.dart';
import 'package:sputnik_app/pages/home/repos/service/repos_repositiry.dart';
import 'package:sputnik_app/utils/logger.dart';

part 'repos_event.dart';
part 'repos_state.dart';

class ReposBloc extends Bloc<ReposEvent, ReposState> {
  ReposProvider reposProvider = ReposProvider();
  ReposBloc({required this.reposProvider}) : super(ReposInitial()) {
    on<GetListReposEvent>(getListRepos);
  }

  FutureOr<void> getListRepos(
      GetListReposEvent event, Emitter<ReposState> emit) async {
    emit(ReposLoading());
    try {
      List<Repos>? getData = await reposProvider.getData();
      if (getData == null) {
        return;
      }

      emit(ReposLoaded(repos: getData));
    } catch (e) {
      logger.e(e);
    }
  }
}
