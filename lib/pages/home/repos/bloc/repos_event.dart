part of 'repos_bloc.dart';

abstract class ReposEvent extends Equatable {
  const ReposEvent();
}

class GetListReposEvent extends ReposEvent {
  @override
  List<Object> get props => [];
}
