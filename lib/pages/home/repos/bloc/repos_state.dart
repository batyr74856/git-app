part of 'repos_bloc.dart';

abstract class ReposState extends Equatable {
  const ReposState();
}

class ReposInitial extends ReposState {
  @override
  List<Object> get props => [];
}

class ReposLoading extends ReposState {
  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class ReposLoaded extends ReposState {
  List<Repos>? repos;
  ReposLoaded({this.repos});
  @override
  List<Object> get props => [];
}
