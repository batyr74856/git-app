import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sputnik_app/pages/home/following/bloc/following_bloc.dart';

import '../../../core/app_colors.dart';
import '../../../core/image_network.dart';
import '../../../translations/locale_keys.g.dart';

class FollowingList extends StatelessWidget {
  const FollowingList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                LocaleKeys.following_you_text.tr(),
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 28,
                  color: AppColors.blackPrimary,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Text(
                LocaleKeys.view_all_text.tr(),
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  fontFamily: 'Poppins',
                  fontSize: 15,
                  color: AppColors.blackPrimary,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 150,
          child: BlocBuilder<FollowingBloc, FollowingState>(
            builder: (context, state) {
              if (state is FollowingInitial) {
                return const SizedBox();
              } else if (state is FollowingLoading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is FollowingLoaded) {
                return Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: state.following!.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.only(right: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Material(
                                elevation: 5,
                                borderRadius: BorderRadius.circular(60),
                                child: Container(
                                  height: 100,
                                  width: 100,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(60)),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(60),
                                    child: BaseNetworkImage(
                                      url: '${state.following![index].avatar}',
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                state.following![index].name!,
                                style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 17,
                                  color: AppColors.blackPrimary,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              Text(
                                '${state.following![index].id!}',
                                style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 10,
                                  color: AppColors.grey,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        );
                      }),
                );
              }
              return const SizedBox();
            },
          ),
        ),
      ],
    );
  }
}
