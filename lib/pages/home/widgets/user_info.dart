import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sputnik_app/core/app_assets.dart';
import 'package:sputnik_app/core/app_colors.dart';
import 'package:sputnik_app/translations/locale_keys.g.dart';

import '../../profile/bloc/profile_bloc.dart';

class UserInfo extends StatelessWidget {
  const UserInfo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BlocBuilder<ProfileBloc, ProfileState>(builder: (context, state) {
          if (state is ProfileInitial) {
            return const SizedBox();
          } else if (state is ProfileLoading) {
            return Center(
                child: CircularProgressIndicator(
              color: AppColors.background,
            ));
          } else if (state is ProfileLoaded) {
            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        state.user!.login!,
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 28,
                          color: AppColors.blackPrimary,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Column(
                        children: [
                          const SizedBox(
                            height: 4,
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 6, vertical: 14),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                gradient: LinearGradient(
                                  begin: Alignment.topRight,
                                  end: Alignment.bottomLeft,
                                  colors: [
                                    AppColors.primary,
                                    AppColors.secondary,
                                  ],
                                )),
                            child: Row(
                              children: [
                                SvgPicture.asset(AppAssets.plus),
                                const SizedBox(
                                  width: 8,
                                ),
                                Text(
                                  LocaleKeys.follow_on_text.tr(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 10,
                                    color: AppColors.white,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            '${LocaleKeys.company_text.tr()} -',
                            style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 17,
                              color: AppColors.grey,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Text(
                            state.user!.company!,
                            style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 17,
                              color: AppColors.grey,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "${LocaleKeys.email_text.tr()} - ",
                            style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 17,
                              color: AppColors.grey,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Text(
                            state.user!.twitterUsername!,
                            style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 17,
                              color: AppColors.grey,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                      Wrap(
                        children: [
                          Text(
                            '${LocaleKeys.bio_text.tr()} - ${state.user!.bio!}',
                            style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 17,
                              color: AppColors.grey,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            );
          }
          return const SizedBox();
        }),
        const SizedBox(
          height: 10,
        ),
        Divider(
          color: AppColors.dividerColor,
          thickness: 2,
          endIndent: 15,
          indent: 15,
        ),
        const SizedBox(
          height: 8,
        ),
      ],
    );
  }
}
