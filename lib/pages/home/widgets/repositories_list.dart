import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sputnik_app/core/app_colors.dart';
import 'package:sputnik_app/translations/locale_keys.g.dart';

import '../../../core/app_assets.dart';
import '../repos/bloc/repos_bloc.dart';

class RepositoriesList extends StatelessWidget {
  const RepositoriesList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Divider(
          color: AppColors.dividerColor,
          thickness: 2,
          endIndent: 15,
          indent: 15,
        ),
        const SizedBox(
          height: 8,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                LocaleKeys.repositories_text.tr(),
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 28,
                  color: AppColors.blackPrimary,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Text(
                LocaleKeys.view_all_text.tr(),
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  fontFamily: 'Poppins',
                  fontSize: 15,
                  color: AppColors.blackPrimary,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 150,
          child: BlocBuilder<ReposBloc, ReposState>(
            builder: (context, state) {
              if (state is ReposInitial) {
                return const SizedBox();
              } else if (state is ReposLoading) {
                return Center(
                  child: CircularProgressIndicator(
                    color: AppColors.background,
                  ),
                );
              } else if (state is ReposLoaded) {
                return Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: state.repos!.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.only(right: 20),
                          child: Row(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Image.asset(AppAssets.image),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    state.repos![index].name!,
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 17,
                                      color: AppColors.blackPrimary,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  Text(
                                    '${state.repos![index].id!}',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 10,
                                      color: AppColors.grey,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 12),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 6, horizontal: 14),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(7),
                                        color:
                                            AppColors.greyBackgroundSecondary,
                                      ),
                                      child: Row(children: [
                                        SvgPicture.asset(AppAssets.star),
                                        const SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          '${state.repos![index].stars!}',
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 10,
                                            color: AppColors.goldColor,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 6, horizontal: 14),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(7),
                                        color: AppColors.blackPrimary,
                                      ),
                                      child: Row(children: [
                                        SvgPicture.asset(AppAssets.fork),
                                        const SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          '${state.repos![index].stars!}',
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 10,
                                            color: AppColors.white,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ]),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        );
                      }),
                );
              }
              return const SizedBox();
            },
          ),
        ),
      ],
    );
  }
}
