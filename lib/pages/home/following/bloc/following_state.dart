part of 'following_bloc.dart';

abstract class FollowingState extends Equatable {
  const FollowingState();
}

class FollowingInitial extends FollowingState {
  @override
  List<Object> get props => [];
}

class FollowingLoading extends FollowingState {
  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class FollowingLoaded extends FollowingState {
  List<Following>? following;
  FollowingLoaded({this.following});
  @override
  List<Object> get props => [];
}
