part of 'following_bloc.dart';

abstract class FollowingEvent extends Equatable {}

class GetListFollowingEvent extends FollowingEvent {
  @override
  List<Object> get props => [];
}
