import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:sputnik_app/data/models/following.dart';
import 'package:sputnik_app/pages/home/following/service/following_repository.dart';
import 'package:sputnik_app/utils/logger.dart';

part 'following_event.dart';
part 'following_state.dart';

class FollowingBloc extends Bloc<FollowingEvent, FollowingState> {
  FollowingProvider followingProvider = FollowingProvider();
  FollowingBloc({required this.followingProvider}) : super(FollowingInitial()) {
    on<GetListFollowingEvent>(getListFollowing);
  }

  FutureOr<void> getListFollowing(
      GetListFollowingEvent event, Emitter<FollowingState> emit) async {
    emit(FollowingLoading());
    try {
      List<Following>? getData = await followingProvider.getData();
      if (getData == null) {
        return;
      }

      emit(FollowingLoaded(following: getData));
    } catch (e) {
      logger.e(e);
    }
  }
}
