import '../../../../data/models/following.dart';
import '../../../../data/network/rest_client.dart';
import '../../../../data/service_locator.dart';
import '../../../../utils/logger.dart';

class FollowingProvider {
  Future<List<Following>?> getData() async {
    try {
      var response = await serviceLocator<RestClient>().getFollowing();
      return response;
    } catch (e) {
      logger.e(e);
    }
    return null;
  }
}
