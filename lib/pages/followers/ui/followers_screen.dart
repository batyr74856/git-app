import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:sputnik_app/core/app_colors.dart';

import 'package:sputnik_app/pages/followers/widgets/followers_list.dart';
import 'package:sputnik_app/pages/followers/widgets/search_followers.dart';

import '../../../translations/locale_keys.g.dart';
import '../bloc/followers_bloc.dart';
import '../service/followers_repository.dart';

// ignore: must_be_immutable
class FollowersScreen extends StatelessWidget {
  FollowersScreen({super.key});
  FollowersProvider followersProvider = FollowersProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppColors.background,
        title: Text(
          LocaleKeys.followers_text.tr(),
          style: TextStyle(
            fontFamily: 'Poppins',
            fontSize: 28,
            color: AppColors.blackPrimary,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          context.read<FollowersBloc>().add(GetFollowersListEvent());
        },
        child: Column(
          children: const [
            SearchFollowersField(),
            SizedBox(
              height: 20,
            ),
            FollowersList()
          ],
        ),
      ),
    );
  }
}
