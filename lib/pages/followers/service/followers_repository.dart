import 'package:sputnik_app/data/models/followers.dart';
import 'package:sputnik_app/utils/logger.dart';

import '../../../data/network/rest_client.dart';
import '../../../data/service_locator.dart';

class FollowersProvider {
  Future<List<Followers>?> getData() async {
    try {
      var response = await serviceLocator<RestClient>().getFollowers();
      return response;
    } catch (e) {
      logger.e(e);
    }
    return null;
  }
}
