part of 'followers_bloc.dart';

abstract class FollowersEvent extends Equatable {}

class GetFollowersListEvent extends FollowersEvent {
  @override
  List<Object> get props => [];
}
