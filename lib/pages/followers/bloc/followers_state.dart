part of 'followers_bloc.dart';

abstract class FollowersState extends Equatable {
  const FollowersState();
}

class FollowersInitial extends FollowersState {
  @override
  List<Object> get props => [];
}

class FollowersLoading extends FollowersState {
  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class FollowersLoaded extends FollowersState {
  List<Followers>? followers;
  FollowersLoaded({this.followers});

  @override
  List<Object> get props => [];
}
