import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:sputnik_app/data/models/followers.dart';
import 'package:sputnik_app/pages/followers/service/followers_repository.dart';
import 'package:sputnik_app/utils/logger.dart';

part 'followers_event.dart';
part 'followers_state.dart';

class FollowersBloc extends Bloc<FollowersEvent, FollowersState> {
  FollowersProvider followersProvider = FollowersProvider();
  FollowersBloc({required this.followersProvider}) : super(FollowersInitial()) {
    on<GetFollowersListEvent>(getFollowers);
  }

  FutureOr<void> getFollowers(
      GetFollowersListEvent event, Emitter<FollowersState> emit) async {
    emit(FollowersLoading());

    try {
      List<Followers>? getData = await followersProvider.getData();
      if (getData == null) {
        return;
      }

      emit(FollowersLoaded(followers: getData));
    } catch (e) {
      logger.e(e);
    }
  }
}
