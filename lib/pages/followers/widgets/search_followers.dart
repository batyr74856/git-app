import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sputnik_app/core/app_assets.dart';
import 'package:sputnik_app/core/app_colors.dart';
import 'package:sputnik_app/translations/locale_keys.g.dart';

class SearchFollowersField extends StatelessWidget {
  const SearchFollowersField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: TextFormField(
        cursorColor: AppColors.blackPrimary,
        style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w500,
            color: AppColors.blackPrimary),
        textAlign: TextAlign.left,
        decoration: InputDecoration(
            suffixIcon: Padding(
              padding: const EdgeInsets.all(14.0),
              child: SvgPicture.asset(
                AppAssets.filter,
              ),
            ),
            contentPadding:
                const EdgeInsets.symmetric(vertical: 10, horizontal: 24),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 1, color: AppColors.grey),
              borderRadius: BorderRadius.circular(10.0),
            ),
            hintText: '${LocaleKeys.search_text.tr()}...',
            hintStyle: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 17,
                fontWeight: FontWeight.w400,
                color: AppColors.greyPrimary),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide.none),
            filled: true,
            fillColor: AppColors.greyBackground),
      ),
    );
  }
}
