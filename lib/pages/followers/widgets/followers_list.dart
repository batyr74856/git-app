import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sputnik_app/core/app_colors.dart';
import 'package:sputnik_app/core/image_network.dart';

import '../bloc/followers_bloc.dart';

class FollowersList extends StatelessWidget {
  const FollowersList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: BlocBuilder<FollowersBloc, FollowersState>(
        builder: (context, state) {
          if (state is FollowersInitial) {
            return const SizedBox();
          } else if (state is FollowersLoading) {
            return const Center(child: CircularProgressIndicator());
          } else if (state is FollowersLoaded) {
            return ListView.builder(
                shrinkWrap: true,
                itemCount: state.followers!.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      Divider(
                        color: AppColors.dividerColor,
                        thickness: 2,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: ListTile(
                            leading: Material(
                              elevation: 5,
                              borderRadius: BorderRadius.circular(60),
                              child: Container(
                                height: 58,
                                width: 58,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(60)),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(60),
                                  child: BaseNetworkImage(
                                    url: '${state.followers![index].avatar}',
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                            ),
                            title: Text(
                              state.followers![index].name!,
                              style: TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 20,
                                color: AppColors.blackPrimary,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            subtitle: Text(
                              '${state.followers![index].id!}',
                              style: TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 17,
                                color: AppColors.subtitle,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            trailing: state.followers![index].name ==
                                        'twerth' ||
                                    state.followers![index].name == 'andrew' ||
                                    state.followers![index].name == 'datapimp'
                                ? Container(
                                    height: 10,
                                    width: 10,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        color: AppColors.secondary),
                                  )
                                : const SizedBox()),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  );
                });
          }
          return const SizedBox();
        },
      ),
    );
  }
}
