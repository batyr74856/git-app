class UnbordingContent {
  String? image;
  String? title;

  UnbordingContent({
    this.image,
    this.title,
  });
}

List<UnbordingContent> contents = [
  UnbordingContent(
    title: 'Фонд поддержки стартапов «Спутник»',
    image: 'assets/images/welcome.png',
  ),
  UnbordingContent(
    title: 'Мы помогаем сфокусироваться на главном — предпринимательстве',
    image: 'assets/images/planet.png',
  ),
  UnbordingContent(
    title: 'Сделано ботаниками для ботаников',
    image: 'assets/images/idea.png',
  ),
];
