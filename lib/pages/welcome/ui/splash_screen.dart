import 'dart:async';

import 'package:flutter/material.dart';

import 'package:sputnik_app/core/app_assets.dart';
import 'package:sputnik_app/main.dart';
import 'package:sputnik_app/pages/welcome/ui/welcome_screen.dart';

import '../../../bottom_navigation_bar .dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        const Duration(seconds: 2),
        () => Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  show ? const Onbording() : const BottomNavigationBarPage(),
            ),
            ModalRoute.withName("/FirstScreen")));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(child: Image.asset(AppAssets.logo)),
      ),
    );
  }
}
