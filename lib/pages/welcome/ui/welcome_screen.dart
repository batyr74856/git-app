// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:sputnik_app/core/app_assets.dart';
import 'package:sputnik_app/core/app_colors.dart';

import '../../../bottom_navigation_bar .dart';
import '../widgets/content_widget.dart';

class Onbording extends StatefulWidget {
  const Onbording({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _OnbordingState createState() => _OnbordingState();
}

class _OnbordingState extends State<Onbording> {
  int currentIndex = 0;
  late PageController _controller;

  @override
  void initState() {
    _controller = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: PageView.builder(
              controller: _controller,
              itemCount: contents.length,
              onPageChanged: (int index) {
                setState(() {
                  currentIndex = index;
                });
              },
              itemBuilder: (_, i) {
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 25),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 50,
                      ),
                      Row(
                        children: [
                          Image.asset(AppAssets.logo),
                        ],
                      ),
                      Expanded(
                        child: Image.asset(
                          contents[i].image!,
                        ),
                      ),
                      SizedBox(
                        width: 260,
                        child: Text(
                          contents[i].title!,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 22,
                            color: AppColors.blackPrimary,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
          SizedBox(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(
                contents.length,
                (index) => buildDot(index, context),
              ),
            ),
          ),
          Container(
            height: 58,
            margin: const EdgeInsets.symmetric(horizontal: 25, vertical: 25),
            width: double.infinity,
            child: InkWell(
              onTap: () {
                if (currentIndex == contents.length - 1) {
                  onDone(context);
                }
                _controller.nextPage(
                  duration: const Duration(milliseconds: 100),
                  curve: Curves.bounceIn,
                );
              },
              child: Container(
                height: 58,
                width: double.infinity,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14),
                    gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [
                        AppColors.primary,
                        AppColors.secondary,
                      ],
                    )),
                child: Text(
                  "Next",
                  style: TextStyle(
                    fontFamily: 'Poppins',
                    fontSize: 17,
                    color: AppColors.white,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Container buildDot(int index, BuildContext context) {
    return Container(
      height: 30,
      width: 30,
      alignment: Alignment.center,
      margin: const EdgeInsets.only(right: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        color: currentIndex == index ? AppColors.white : AppColors.black,
      ),
      child: Text(
        '${index + 1}',
        style: TextStyle(
          fontFamily: 'Poppins',
          fontSize: 13,
          color: currentIndex == index ? AppColors.black : AppColors.white,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }

  void onDone(context) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool('ON_BOARDING', false);
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => const BottomNavigationBarPage()));
  }
}
