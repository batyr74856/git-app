// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader {
  const CodegenLoader();

  @override
  // ignore: avoid_renaming_method_parameters
  Future<Map<String, dynamic>> load(String fullPath, Locale locale) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String, dynamic> ru = {
    "sputnik_text": "Фонд поддержки стартапов «Спутник»",
    "help_text": "Мы помогаем сфокусироваться на главном — предпринимательстве",
    "make_text": "Сделано ботаниками для ботаников",
    "next_text": "Следующий",
    "github_text": "GitHub social",
    "enter_nickname_text": "Введите ник в github",
    "enter_nickname_on_text": "Введите ник",
    "search_text": "Поиск",
    "sign_in_text": "Я соглашаюсь с",
    "term_text": "Условиями использования",
    "and_text": "и",
    "policy_text": "Политикой конфиденциальности",
    "follow_on_text": "Подписаться в github",
    "company_text": "Компания",
    "email_text": "Почта",
    "bio_text": "Био",
    "following_you_text": "Ваши подписки",
    "view_all_text": "Посмотреть все",
    "repositories_text": "Репозитории",
    "followers_text": "Подписчики",
    "my_following_text": "Мои подписки",
    "my_followers_text": "Мои подписчики",
    "my_badges_text": "Мои значки",
    "my_organizatios_text": "Моя организация",
    "home_text": "Главная",
    "chat_text": "Чат",
    "profile_text": "Профиль",
    "error_first_text": "Пользователь с таким именем",
    "error_second_text": "не найден!"
  };
  static const Map<String, dynamic> en = {
    "sputnik_text": "Startup support fund  «Спутник»",
    "help_text": "We help you focus on what matters most - entrepreneurship",
    "make_text": "Made by nerds for nerds",
    "next_text": "Next",
    "github_text": "GitHub social",
    "enter_nickname_text": "Enter nickname on github",
    "enter_nickname_on_text": "Enter nickname ",
    "search_text": "Search",
    "sign_in_text": "By signing in, I agree with",
    "term_text": "Terms of Use",
    "and_text": "and",
    "policy_text": "Privacy Policy",
    "follow_on_text": "Follow on github",
    "company_text": "Company",
    "email_text": "Email",
    "bio_text": "Bio",
    "following_you_text": "Following you",
    "view_all_text": "View all",
    "repositories_text": "Repositories",
    "followers_text": "Followers",
    "my_following_text": "My following",
    "my_followers_text": "My followers",
    "my_badges_text": "My badges",
    "my_organizatios_text": "My organization",
    "home_text": "Home",
    "chat_text": "Chat",
    "profile_text": "Profile",
    "error_first_text": "User with this nickname",
    "error_second_text": "not found!"
  };
  static const Map<String, Map<String, dynamic>> mapLocales = {
    "ru": ru,
    "en": en
  };
}
