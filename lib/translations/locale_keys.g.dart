// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class LocaleKeys {
  static const sputnik_text = 'sputnik_text';

  static const help_text = 'help_text';
  static const make_text = 'make_text';
  static const next_text = 'next_text';
  static const github_text = 'github_text';
  static const enter_nickname_text = 'enter_nickname_text';
  static const enter_nickname_on_text = 'enter_nickname_on_text';
  static const search_text = 'search_text';
  static const sign_in_text = 'sign_in_text';
  static const term_text = 'term_text';
  static const and_text = 'and_text';
  static const policy_text = 'policy_text';
  static const follow_on_text = 'follow_on_text';
  static const company_text = 'company_text';
  static const email_text = 'email_text';
  static const bio_text = 'bio_text';
  static const following_you_text = 'following_you_text';
  static const view_all_text = 'view_all_text';
  static const repositories_text = 'repositories_text';
  static const followers_text = 'followers_text';
  static const my_following_text = 'my_following_text';
  static const my_followers_text = 'my_followers_text';
  static const my_badges_text = 'my_badges_text';
  static const my_organizatios_text = 'my_organizatios_text';
  static const home_text = 'home_text';
  static const chat_text = 'chat_text';
  static const profile_text = 'profile_text';
  static const error_first_text = 'error_first_text';
  static const error_second_text = 'error_second_text';
}
