import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:sputnik_app/core/env.dart';
import 'package:sputnik_app/data/models/followers.dart';
import 'package:sputnik_app/data/models/following.dart';
import 'package:sputnik_app/data/models/repos.dart';
import 'package:sputnik_app/data/models/user.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: apiURL)
abstract class RestClient {
  factory RestClient(Dio dio, {String? baseUrl}) = _RestClient;

  @GET('/users/GantMan')
  Future<User?> getProfile();

  @GET('/users/GantMan/followers')
  Future<List<Followers>> getFollowers();

  @GET('/users/GantMan/following')
  Future<List<Following>> getFollowing();

  @GET('/users/GantMan/repos')
  Future<List<Repos>> getRepos();
}
