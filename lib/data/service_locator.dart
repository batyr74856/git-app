import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

import '../core/env.dart';
import 'network/rest_client.dart';

GetIt serviceLocator = GetIt.instance;

void serviceLocatorSetup() {
  serviceLocator.registerFactory<RestClient>(() {
    final dio = Dio();

    return RestClient(dio, baseUrl: apiURL);
  });
}
