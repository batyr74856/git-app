import 'package:json_annotation/json_annotation.dart';

part 'following.g.dart';

@JsonSerializable()
class Following {
  @JsonKey(name: 'login')
  String? name;
  int? id;
  @JsonKey(name: 'avatar_url')
  String? avatar;

  Following({this.name, this.avatar, this.id});

  factory Following.fromJson(Map<String, dynamic> json) =>
      _$FollowingFromJson(json);

  Map<String, dynamic> toJson() => _$FollowingToJson(this);
}
