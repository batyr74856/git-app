// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'repos.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Repos _$ReposFromJson(Map<String, dynamic> json) => Repos(
      name: json['name'] as String?,
      id: json['id'] as int?,
      forks: json['forks_count'] as int?,
      stars: json['stargazers_count'] as int?,
    );

Map<String, dynamic> _$ReposToJson(Repos instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'forks_count': instance.forks,
      'stargazers_count': instance.stars,
    };
