import 'package:json_annotation/json_annotation.dart';

part 'repos.g.dart';

@JsonSerializable()
class Repos {
  String? name;
  int? id;
  @JsonKey(name: 'forks_count')
  int? forks;
  @JsonKey(name: 'stargazers_count')
  int? stars;

  Repos({this.name, this.id, this.forks, this.stars});

  factory Repos.fromJson(Map<String, dynamic> json) => _$ReposFromJson(json);

  Map<String, dynamic> toJson() => _$ReposToJson(this);
}
