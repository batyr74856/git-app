// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'followers.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Followers _$FollowersFromJson(Map<String, dynamic> json) => Followers(
      name: json['login'] as String?,
      avatar: json['avatar_url'] as String?,
      id: json['id'] as int?,
    );

Map<String, dynamic> _$FollowersToJson(Followers instance) => <String, dynamic>{
      'login': instance.name,
      'id': instance.id,
      'avatar_url': instance.avatar,
    };
