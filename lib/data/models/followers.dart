import 'package:json_annotation/json_annotation.dart';

part 'followers.g.dart';

@JsonSerializable()
class Followers {
  @JsonKey(name: 'login')
  String? name;
  int? id;
  @JsonKey(name: 'avatar_url')
  String? avatar;

  Followers({this.name, this.avatar, this.id});

  factory Followers.fromJson(Map<String, dynamic> json) =>
      _$FollowersFromJson(json);

  Map<String, dynamic> toJson() => _$FollowersToJson(this);
}
