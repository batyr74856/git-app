// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'following.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Following _$FollowingFromJson(Map<String, dynamic> json) => Following(
      name: json['login'] as String?,
      avatar: json['avatar_url'] as String?,
      id: json['id'] as int?,
    );

Map<String, dynamic> _$FollowingToJson(Following instance) => <String, dynamic>{
      'login': instance.name,
      'id': instance.id,
      'avatar_url': instance.avatar,
    };
